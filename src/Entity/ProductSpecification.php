<?php

namespace App\Entity;

use App\Repository\ProductSpecificationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ProductSpecificationRepository::class)
 */
class ProductSpecification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"product"})
     */
    private ?string $value;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="specifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Product $product;

    /**
     * @ORM\ManyToOne(targetEntity=ProductSpecificationCategory::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"product"})
     */
    private ?ProductSpecificationCategory $category;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCategory(): ?ProductSpecificationCategory
    {
        return $this->category;
    }

    public function setCategory(?ProductSpecificationCategory $category): self
    {
        $this->category = $category;

        return $this;
    }
}
