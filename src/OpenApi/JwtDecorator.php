<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\OpenApi;
use ArrayObject;

class JwtDecorator implements OpenApiFactoryInterface
{
    /**
     * @var OpenApiFactoryInterface
     */
    private OpenApiFactoryInterface $decorated;

    public function __construct(OpenApiFactoryInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Token'] = [
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true
                ]
            ]
        ];
        $schemas['Credentials'] = [
            'type' => 'object',
            'properties' => [
                'apiKey' => [
                    'type' => 'string',
                    'example' => '$2y$10$F1cRE7.INiq9E.ekV6nmh.6G2/G8lOY3DXwz9g8ghXOB/ZokfijKG'
                ],
                'apiSecret' => [
                    'type' => 'string',
                    'example' => 'password'
                ]
            ]
        ];

        $pathItem = new PathItem(
            'JWT Token',
            'Get JWT token to login.',
            'Generate new JWT Token',
            null,
            null,
            new Operation(
                'postCredentialsItem',
                [],
                [
                    '200' => [
                        'description' => 'Get JWT token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token',
                                ],
                            ],
                        ],
                    ],
                ],
                '',
                '',
                null,
                [],
                new RequestBody(
                    '',
                    new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $openApi->getPaths()->addPath('/login_check', $pathItem);
        return $openApi;
    }
}
