<?php

namespace App\Repository;

use App\Entity\ProductSpecificationCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductSpecificationCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductSpecificationCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductSpecificationCategory[]    findAll()
 * @method ProductSpecificationCategory[]    findBy(array $criteria, array $orderBy=null, $limit=null, $offset=null)
 */
class ProductSpecificationCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductSpecificationCategory::class);
    }

    // /**
    //  * @return ProductSpecificationCategory[] Returns an array of ProductSpecificationCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductSpecificationCategory
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
