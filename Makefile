db:
	php bin/console d:d:d --if-exists -f
	php bin/console d:d:c
	php bin/console d:m:m -n
	php bin/console hautelook:fixtures:load -n

db-test:
	php bin/console d:d:d --if-exists -f -e test
	php bin/console d:d:c -e test
	php bin/console d:m:m -n -e test
	php bin/console hautelook:fixtures:load -n -e test

tests:
	php bin/console d:d:d --if-exists -f -e test
	php bin/console d:d:c -e test
	php bin/console d:m:m -n -e test
	php bin/console hautelook:fixtures:load -n -e test
	php bin/console c:c -e test
	php bin/phpunit
