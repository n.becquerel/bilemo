<?php

namespace App\Tests\Functionnal;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Tests\Traits\WithAuthenticatedUserTrait;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class ProductResourceTest extends ApiTestCase
{
    use RefreshDatabaseTrait;
    use WithAuthenticatedUserTrait;

    public function testGetCollection()
    {
        $token = $this->getUserToken();

        $response = static::createClient()->request('GET', '/api/products', [
            'headers' => [
                'Accept' => 'application/hal+json',
            ],
            'auth_bearer' => $token
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');

        $this->assertJsonContains([
            '_links' => [
                'self' => ['href' => '/api/products?page=1'],
                'first' => ['href' => '/api/products?page=1'],
                'last' => ['href' => '/api/products?page=7'],
                'next' => ['href' => '/api/products?page=2'],
            ],
            'totalItems' => 200,
            'itemsPerPage' => 30
        ]);
        
        $this->assertTrue(array_key_exists('_embedded', $response->toArray()));
        $firstItem = $response->toArray()['_embedded']['item'][0];
        $this->assertTrue(array_key_exists('_links', $firstItem));
        $this->assertTrue(array_key_exists('name', $firstItem));
        $this->assertTrue(array_key_exists('description', $firstItem));
        $this->assertTrue(array_key_exists('photos', $firstItem));
        $this->assertTrue(array_key_exists('specifications', $firstItem));
    }

    public function testGetItem()
    {
        $token = $this->getUserToken();
        $response = static::createClient()->request('GET', '/api/products/1', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);
        
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');

        $this->assertJsonContains([
            '_links' => [
                'self' => [
                    'href' => '/api/products/1'
                ]
            ],
        ]);
        $this->assertTrue(array_key_exists('name', $response->toArray()));
        $this->assertTrue(array_key_exists('description', $response->toArray()));
        $this->assertTrue(array_key_exists('photos', $response->toArray()));
        $this->assertTrue(array_key_exists('specifications', $response->toArray()));
    }

    public function testProductResourcesNeedsAuthentication()
    {
        self::createClient()->request('GET', '/api/products', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);
        
        self::createClient()->request('GET', '/api/products/1', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);
    }

//    public function testPostCollection()
//    {
//        $response = static::createClient()->request('POST', '/api/products', [
//            'headers' => [
//                'Accept' => 'application/hal+json'
//            ]
//        ]);
//        $this->assertResponseStatusCodeSame(405);
//        $this->assertResponseHeaderSame('content-type', 'application/hal+json');
//    }

//    public function testUpdateItem()
//    {
//        static::createClient()->request('PUT', '/api/products/1');
//        $this->assertResponseStatusCodeSame(405);
//        $this->assertResponseHeaderSame('content-type', 'application/ld+json');
//    }
//
//    public function testDeleteItem()
//    {
//        static::createClient()->request('DELETE', '/api/products/1');
//        $this->assertResponseStatusCodeSame(405);
//        $this->assertResponseHeaderSame('content-type', 'application/ld+json');
//    }
}
