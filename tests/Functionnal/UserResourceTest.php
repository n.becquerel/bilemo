<?php

namespace App\Tests\Functionnal;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use App\Tests\Traits\WithAuthenticatedUserTrait;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class UserResourceTest extends ApiTestCase
{
    use RefreshDatabaseTrait;
    use WithAuthenticatedUserTrait;

    public function testUserGetCollection()
    {
        $token = $this->getUserToken();
        $client = self::createClient();
        $response = $client->request('GET', '/api/users', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');

        // Asserts that the returned JSON is a superset of this one
        $this->assertJsonContains([
            '_links' => [
                'self' => ['href' => '/api/users'],
            ],
            // Authenticated customer only have 20 users so we remove pagination (since 30 itemsPerpage)
            'totalItems' => 20,
            'itemsPerPage' => 30
        ]);
        $this->assertTrue(array_key_exists('_embedded', $response->toArray()));
        $this->assertTrue(array_key_exists('item', $response->toArray()['_embedded']));
        $this->assertTrue(array_key_exists('_links', $response->toArray()['_embedded']['item'][0]));
        $this->assertTrue(array_key_exists('firstname', $response->toArray()['_embedded']['item'][0]));
        $this->assertTrue(array_key_exists('lastname', $response->toArray()['_embedded']['item'][0]));
    }

    public function testUserGetItem()
    {
        $token = $this->getUserToken();
        $response = self::createClient()->request('GET', '/api/users/1', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');

        $this->assertJsonContains([
            '_links' => ['self' => ['href' => '/api/users/1']],
            'id' => 1,
        ]);

        $this->assertTrue(array_key_exists('firstname', $response->toArray()));
        $this->assertTrue(array_key_exists('lastname', $response->toArray()));
    }

    public function testUserGetItemWrongOwner()
    {
        $token = $this->getUserToken();
        self::createClient()->request('GET', '/api/users/21', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);
        $this->assertResponseStatusCodeSame(404);
        $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');
    }

    /**
     * @dataProvider provideUserDataForPostCollection
     */
    public function testUserPostCollection($data)
    {
        $token = $this->getUserToken();
        self::createClient()->request('POST', '/api/users', [
            'json' => $data['data'],
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);

        if (isset($data['errors'])) {
            $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');
            $this->assertResponseStatusCodeSame(422);
            $this->assertJsonContains([
                'title' => 'Validation Failed',
                'violations' => [
                    $data['errors']
                ]
            ]);
        } else {
            $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');
            $this->assertResponseStatusCodeSame(201);
        }
    }

    public function provideUserDataForPostCollection()
    {
        return [
            [
                [
                    'data' => [
                        'firstname' => '',
                        'lastname' => 'test'
                    ],
                    'errors' => [
                        'propertyPath' => 'firstname',
                        'title' => 'This value should not be blank.'
                    ]
                ]
            ],
            [
                [
                    'data' => [
                        'firstname' => 'test1',
                        'lastname' => ''
                    ],
                    'errors' => [
                        'propertyPath' => 'lastname',
                        'title' => 'This value should not be blank.'
                    ]
                ]
            ],
            [
                [
                    'data' => [
                        'firstname' => '',
                        'lastname' => ''
                    ],
                    'errors' => [
                        'propertyPath' => 'firstname',
                        'title' => 'This value should not be blank.'
                    ],
                    [
                        'propertyPath' => 'lastname',
                        'title' => 'This value should not be blank.'
                    ]
                ]
            ],
            [
                [
                    'data' => [
                        'firstname' => 'test',
                        'lastname' => 'test'
                    ]
                ]
            ]
        ];
    }

    /**
     * @dataProvider provideUserDataForPostCollection
     */
    public function testUserPutItem($data)
    {
        $token = $this->getUserToken();
        self::createClient()->request('PUT', '/api/users/1', [
            'json' => $data['data'],
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);

        if (isset($data['errors'])) {
            $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');
            $this->assertResponseStatusCodeSame(422);
            $this->assertJsonContains([
                'title' => 'Validation Failed',
                'violations' => [
                    $data['errors']
                ]
            ]);
        } else {
            $this->assertResponseHeaderSame('content-type', 'application/hal+json; charset=utf-8');
            $this->assertResponseIsSuccessful();
        }
    }

    public function testUserPutItemWrongOwner()
    {
        $token = $this->getUserToken();
        self::createClient()->request('PUT', '/api/users/21', [
            'json' => ['firstname' => 'test', 'lastname' => 'test'],
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);

        $this->assertResponseStatusCodeSame(404);
    }

    public function testUserDelete()
    {
        $token = $this->getUserToken();
        self::createClient()->request('DELETE', '/api/users/1', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);
        $this->assertResponseStatusCodeSame(204);

        $this->assertNull(self::$container->get('doctrine')->getRepository(User::class)->find(1));
    }

    public function testUserDeleteWrongOwner()
    {
        $token = $this->getUserToken();
        self::createClient()->request('DELETE', '/api/users/21', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ],
            'auth_bearer' => $token
        ]);
        $this->assertResponseStatusCodeSame(404);
    }

    public function testUserResourcesNeedAuthentication()
    {
        self::createClient()->request('GET', '/api/users', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);
        
        self::createClient()->request('POST', '/api/users', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);
        
        self::createClient()->request('GET', '/api/users/1', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);
        
        self::createClient()->request('DELETE', '/api/users/1', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);
        
        self::createClient()->request('PUT', '/api/users/1', [
            'headers' => [
                'Accept' => 'application/hal+json'
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);
    }
}
