<?php

namespace App\Tests\Functionnal;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class JWTLoginCheckTest extends ApiTestCase
{
    public function testApiLogin()
    {
        $response = self::createClient()->request('POST', '/login_check', [
            'json' => [
                'apiKey' => '$2y$10$EkkIGjUVkLAhlFW0T8sPB.stH8mojjMzM0AvbXi/rkXfcL0SzBlby',
                'apiSecret' => 'password'
            ],
            'headers' => ['Accept' => 'application/hal+json']
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertTrue(array_key_exists('token', $response->toArray()));
        $token = $response->toArray()['token'];

        self::createClient()->request('GET', '/api/products');
        $this->assertResponseStatusCodeSame(401);

        self::createClient()->request('GET', '/api/products', [
//            'auth_bearer' => $token
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }
}
