<?php

namespace App\Tests\Traits;

trait WithAuthenticatedUserTrait
{
    /**
     * Provide token of authenticated user for tests
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function getUserToken()
    {
        $response = static::createClient()->request('POST', '/login_check', [
            'json' =>  [
                'apiKey' => '$2y$10$EkkIGjUVkLAhlFW0T8sPB.stH8mojjMzM0AvbXi/rkXfcL0SzBlby',
                'apiSecret' => 'password'
            ],
            'headers' => [
                'Accept' => 'application/hal+json'
            ]
        ]);

        return $response->toArray()['token'];
    }
}
