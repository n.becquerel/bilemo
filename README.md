# BileMo

[![pipeline status](https://gitlab.com/n.becquerel/bilemo/badges/master/pipeline.svg)](https://gitlab.com/n.becquerel/bilemo/-/commits/master)
[![coverage report](https://gitlab.com/n.becquerel/bilemo/badges/master/coverage.svg)](https://gitlab.com/n.becquerel/bilemo/-/commits/master)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ed7b595bcd5c4f8fa68534f1bbf8fd0a)](https://www.codacy.com/gl/n.becquerel/bilemo/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=n.becquerel/bilemo&amp;utm_campaign=Badge_Grade)

## Installation  
To install the project, after cloning the repository install php dependencies

```bash
composer install
```
Then create you .env.local at project root and add informations about you database connexion.

Next generate keypair for Lexik Jwt Bundle to work properly by running this command
```bash
php bin/console lexik:jwt:generate-keypair
```

You can now create database and load fixtures by running
```bash
php bin/console doctrine:database:create -n \
    && php bin/console doctrine:migrations:migrate -n \
    && php bin/console hautelook:fixtures:load -n
```
If you system can run Makefiles you just have to execute
```bash
make db
```

### Running tests

To run tests we have to create database and load fixtures for "test" environment
```bash
php bin/console doctrine:database:create -n -e test \
    && php bin/console doctrine:migrations:migrate -n -e test \
    && php bin/console hautelook:fixtures:load -n -e test
```

You can then run 
```bash
php bin/phpunit 
```

### Api Documentation

Once project is installed and running successfully, docs are available at "/api/docs".
